module.exports = async(options = {}) => {
    let { rootEl, $, wrapper, region } = options
    let links = rootEl.find('a').toArray()

    // here we have a list will a lot of links (a tag)
    // departments links have innerHTML containing a number (i.e: 01 - Ain)
    // lets use that knowledge to process the data

    let departments = []
        // we use a cursor to add villages into each department
    let currentDepartment = null

    // for testing (less items)
    // links = links.splice(0, 5)

    links.forEach(link => {
        let contentIsNaN = isNaN(parseInt($(link).html()))
        let isDepartment = !contentIsNaN
        if (isDepartment) {
            currentDepartment = {
                title: $(link).html(),
                villages: []
            }
            departments.push(currentDepartment)
        } else {
            currentDepartment.villages.push({
                title: $(link).html(),
                href: $(link).attr('href'),
                amaps: []
            })
        }
    })

    const sequential = require('promise-sequential')

    // Again, we need to go deep and populate villages in each department
    departments = await sequential(
        departments.map(department => {
            return () =>
                (async() => {
                    // Here, we are saving to file system from time to time
                    // before, this was done after collecting amaps from each village
                    // but saving to often corrupt files (async problem)
                    // let's save once per deparment
                    let amapScript = await require('./scrapAvenirBioAmaps')
                    let collectedAmapsCount = Object.keys(amapScript.outputObject).length
                        // we avoid saving 0 items from memory at start
                    if (collectedAmapsCount > 0) {
                        await amapScript.saveOutputToFileSystem()
                        console.log(`Saving ${collectedAmapsCount} entries (json and csv)`)
                    }

                    let villages = []

                    try {
                        villages = await populateVillagesWithAmaps(
                            // department.villages.splice(0, 2), //for teting
                            department.villages,
                            department.title
                        )
                    } catch (err) {
                        console.error(err.stack)
                    }

                    return {
                        title: department.title,
                        villages
                    }
                })()
        })
    )

    return departments

    async function populateVillagesWithAmaps(villages, department) {
        return await sequential(
            villages.map(village => {
                return () =>
                    (async() => {
                        let domain = 'https://www.avenir-bio.fr'
                        let href = village.href.split(domain).join('')
                        return {
                            title: village.title,
                            amaps: await require('./scrapAvenirBioAmaps')({
                                url: `${domain}/${href}`,
                                wrapper,

                                // we pass by the data we have so far in order to achieve a flatten item
                                region,
                                department,
                                village: village.title
                            })
                        }
                    })()
            })
        )
    }
}