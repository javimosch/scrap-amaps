module.exports = async(options = {}) => {
    let { wrapper } = options

    let globalListUrl = 'https://www.avenir-bio.fr/annuaire_amap.php'

    await wrapper.goto(globalListUrl)

    // Find the first dom element who contents (innerHTML) contains the follow string
    let { item, $ } = await wrapper.findByContent('Cliquez sur votre')

    // From there, retrieve all the regions directly
    let regions = item
        .parent()
        .parent()
        .find('h3 > a')
        .toArray()
        .map(el => ({
            title: $(el).html(),
            // we need to access to the department root dom el
            // h2
            //  a
            // div <-- is this one
            // so going to parent and doing next will do the job (jquery syntaxis)
            deparmentsRootEl: $(
                $(el)
                .parent()
                .next()
            ),
            // we will need this one later to access methods like .html() on each item
            $
        }))

    // testing (try 2 regions only)
    // regions = regions.splice(0, 2)

    // now lets scrap region departments in an async, sequential way
    // https://www.npmjs.com/package/promise-sequential
    // is the same as Promise.all but it works sequentially !!
    const sequential = require('promise-sequential')
    regions = await sequential(
        regions.map(region => {
            return () =>
                (async() => {
                    return {
                        title: region.title,
                        departments: await require('./scrapAvenirBioDepartments')({
                            region: region.title,
                            rootEl: region.deparmentsRootEl,
                            $: region.$,
                            wrapper
                        })
                    }
                })()
        })
    )

    return regions
        // For this simple task, we can query the entire html directly
        // console.log(
        //  'France regions (simplier way)',
        // (await wrapper.queryAll('h3 > a')).map(el => el.html())
        // )
}