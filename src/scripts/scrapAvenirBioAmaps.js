let outputObject = {}

module.exports = async(options = {}) => {
    let { wrapper, url, region, department, village } = options

    if (await shouldSkipScraping(process.env.CACHE_LEVEL, outputObject)) {
        console.log('Skipping', region, department, village, url)
        return Object.keys(outputObject)
            .map(key => {
                return outputObject[key]
            })
            .filter(item => item.village == village)
    }

    async function shouldSkipScraping(cacheLevel, output = {}) {
        // Skip scraping the amaps if the village is already present
        if (cacheLevel === 'village') {
            if (
                Object.keys(output).length > 0 &&
                Object.keys(output).find(key => {
                    return output[key].village === village
                })
            ) {
                return true
            }
        }
        return false
    }

    let results = []

    await wrapper.goto(url)

    try {
        await scrapUsingMethodOne()
    } catch (err) {
        console.log(err.stack.substring(0, 100) + '...')
    }

    if (results.length === 0) {
        // let's go brute!
        try {
            await scrapUsingBruteMethod()
        } catch (err) {
            console.log(err.stack.substring(0, 100) + '...')
        }
    }

    if (results.length === 0) {
        // we could save those into another file to test and check what's wrong: no-results.json
        // console.log('No results for', url)
        return [] // return it right away, there is nothing there
    }

    // debug print
    // var jsome = require('jsome')
    // jsome(results)

    // At this point, we have a flat item with keys:
    // region,department,village, title (amap) and email(amap)
    await copyResultToOutputObject(outputObject, results)
    console.log(
        `TOTAL -> ${Object.keys(outputObject).length}`.padEnd(20, ' '),
        region.padEnd(20, ' '),
        department.padEnd(20, ' '),
        village.padEnd(20, ' '),
        'Found',
        results.length,
        'amaps'
    )

    return results

    async function scrapUsingBruteMethod() {
        const { items, $ } = await wrapper.queryAll('body', {
            waitContent: 'mailto:'
        })
        items
            .find(`a`)
            .toArray()
            .filter(
                a =>
                $(a)
                .attr('href')
                .indexOf('mailto:') !== -1
            )
            .forEach(a => {
                results.push({
                    region,
                    department,
                    village,
                    title: $(a).html(),
                    email: $(a)
                        .attr('href')
                        .split('mailto:')
                        .join('')
                })
            })
    }

    async function scrapUsingMethodOne() {
        // The amap site
        // body
        //  div
        //      col
        //          amap item <-- we need to parse this item
        //      col
        //          amap item
        //  div (footer)
        //      col
        //      col
        //      col
        //      col
        let { items, $ } = await wrapper.queryAll('body')
        let footer = false
        let cursor = 3
        do {
            let div = items.find(`div:nth-child(${cursor})`)
            let cols = div.find('.row > div').toArray()
            if (cols.length !== 2) {
                footer = true
            } else {
                results.push(parseItem($(cols[0]), $))
                results.push(parseItem($(cols[1]), $))
                cursor++
            }
        } while (!footer)
        results = results.filter(item => !!item.email)

        function parseItem(el, $) {
            let item = {
                region,
                department,
                village,
                title: el
                    .find('a')
                    .first()
                    .attr('title'),
                email: el
                    .find('a')
                    .toArray()
                    .filter(
                        a =>
                        $(a)
                        .attr('href')
                        .indexOf('mailto') !== -1
                    )
                    .map(el =>
                        $(el)
                        .attr('href')
                        .split('mailto:')
                        .join('')
                    )[0]
                    // html: el.html()
            }

            return item
        }
    }
}

module.exports.outputObject = outputObject
module.exports.saveOutputToFileSystem = saveOutputToFileSystem

async function saveOutputToFileSystem() {
    let output = outputObject
    await saveJSONFile(output)
    await saveCSVFile(output)
}

async function copyResultToOutputObject(output = {}, results) {
    if (Object.keys(output).length === 0) {
        // We only open the file once, when length is 0
        try {
            const filePath = require('path').join(process.cwd(), 'output.json')
            let loaded = JSON.parse(
                (await require('sander').readFile(filePath)).toString('utf-8')
            )
            Object.assign(output, loaded)
            console.log(Object.keys(loaded).length, 'Existing entries loaded!')
        } catch (err) {
            // if it fails to open the file it means doesn't exist yet?
            // (maybe there is a parse error as well, but lets ignore that for now)
            console.log('ERR', err.stack)
        }
    }
    // lets use a dictionary (object) for faster matching and let's use the email in base64 as unique identifier.
    results.forEach(item => {
        output[require('btoa')(item.email)] = item
    })
}

async function saveJSONFile(output = {}) {
    const filePath = require('path').join(process.cwd(), 'output.json')
    await require('sander').writeFile(filePath, JSON.stringify(output, null, 4))
}

async function saveCSVFile(output = {}) {
    const filePath = require('path').join(process.cwd(), 'output.csv')
        // here we can convert that json into a CSV, maybe? :)
        // first lets convert the object into an array
    let ouputAsArray = Object.keys(output).map(key => output[key])
        // now, lets convert it and save it
    let outputAsCSVRaw = ConvertToCSV(ouputAsArray)
    await require('sander').writeFile(filePath, outputAsCSVRaw)

    function ConvertToCSV(objArray) {
        var array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray
        var str = ''

        for (var i = 0; i < array.length; i++) {
            var line = ''
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index]
            }

            str += line + '\r\n'
        }

        return str
    }
}