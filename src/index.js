
let argv = require('./argv')

let puppeteer = require('./puppeteer')

//We grab the headless argument we received from command line and pass it to 
//puppeteer as true/false
puppeteer.setPuppeteerOptions({
    headless: argv.getValue('headless').toString() !== '0'
})

test()

async function test() {

    //Grab the wrapper to simplify the scrapping
    let wrapper = await puppeteer.getWrapper()

    // Scrap single city amaps (title and email) example
    let amaps = await require('./scripts/scrapCityAmaps')({
        url: 'https://www.avenir-bio.fr/amap,herault,34,montpellier.html',
        wrapper
    })

    console.log('AMAPS', amaps)

    //we close the browser :)
    puppeteer.closeBrowser()
}