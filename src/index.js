//
require('events').EventEmitter.prototype._maxListeners = 100

let argv = require('./argv')

let puppeteer = require('./puppeteer')

// We grab the headless argument we received from command line and pass it to
// puppeteer as true/false
puppeteer.setPuppeteerOptions({
    headless: argv.getValue('headless').toString() !== '0'
})

test()

async function test() {
    // Grab the wrapper to simplify the scrapping
    let wrapper = await puppeteer.getWrapper()

    await require('./scripts/scrapAvenirBio')({
        wrapper
    })

    // testing a single village allow us to improve email scrapping when doesnt work
    // await require('./scripts/scrapAvenirBioAmaps')({
    // url: `https://www.avenir-bio.fr/amap,vaucluse,84,villelaure.html`,
    // wrapper,
    // region: 'TEST',
    // department: 'TEST',
    // village: 'TEST'
    // })

    // we close the browser :)
    puppeteer.closeBrowser()
}